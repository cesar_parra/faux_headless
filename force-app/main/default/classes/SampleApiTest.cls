@IsTest(IsParallel=true)
class SampleApiTest {
    @IsTest
    private static void canExecute() {
        try {
            SampleApi.execute();
        } catch (Exception e) {
            System.assert(false, 'Did not expect an error to occur');
        }
    }
}
